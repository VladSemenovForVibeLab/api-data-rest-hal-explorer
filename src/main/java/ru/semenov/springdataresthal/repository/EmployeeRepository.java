package ru.semenov.springdataresthal.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.semenov.springdataresthal.model.Employee;

import java.util.List;

/**
 * CrudRepository <->
 */
public interface EmployeeRepository extends PagingAndSortingRepository<Employee,Long> {
    List<Employee> findByName(String name);
}
